var app = {
  destinationId: '2fc884cb-e5a5-4631-aa55-67b18d52b548',
  schemaUri: 'https://schema.fitko.de/fim/s00000092_1.0.schema.json',
  ars: '064350014014',
  leikaKey: '99123456760610',
  services: [],
  form: null,
  activeTab: 'schema-uri'
};

const uuidv4Regex = new RegExp(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i);
const leikaRegex = new RegExp(/^[0-9]{14}$/i);
const arsRegex = new RegExp(/^[0-9]{0,12}$/i);

function selectTab(tabId) {
  console.log(tabId);
  app.activeTab = tabId;
  app.services = [];
  app.form = null;
}

function findPublicServiceByDestinationId(destinationId) {
  console.log(destinationId);

  if (destinationId.match(uuidv4Regex) == null) {
    console.log('invalid uuid');
    return;
  }

  fetch('./api/v1/destinations/' + destinationId).then(response => response.json()).then(response => {
    if (response.status == 'ok') {
      console.log('found');
      app.services = response.destination.services;
    } else if (response.code == 'destination-not-found') {
      alert('Diese Destination existiert nicht.')
    }
  });
}

function findPublicServiceByRoutingApi() {
  console.log(app.leikaKey, app.ars);

  if (app.leikaKey.match(leikaRegex) == null) {
    console.log('invalid leika key');
  }

  if (app.ars.match(arsRegex) == null) {
    console.log('invalid ARS');
  }

  fetch('./api/v1/routes?ars=' + app.ars + '&leikaKey=' + app.leikaKey).then(response => response.json()).then(response => {
    if (response.count == 1) {
      console.log('found', response);

      findPublicServiceByDestinationId(response.routes[0].destinationId);

    } else if (response.count == 0) {
      alert('Keine Routinginformationen gefunden')
    } else {
      alert('Es wurden mehrere Routinginformationen gefunden. NOT_IMPLEMENTED_ERROR')
    }

  });
}

function selectService(service) {
  console.log(service, this);
  if (service.submissionSchemas.length != 1) {
    console.error("submissionSchemas.length != 1");
    return;
  }

  renderSchemaByUri(service.submissionSchemas[0].schemaUri);
}

function renderSchemaByUri(schemaUri) {
  app.form = {};
  app.form.schemaUri = schemaUri;

  if (app.form.schemaUri.startsWith('https://schema.fitko.de/fim/')) {
    fetch(app.form.schemaUri.toLowerCase()).then(response => response.json()).then(response => {
      app.form.schema = response;
      console.log(response);


      const Form = JSONSchemaForm.default;
      delete app.form.schema['$schema'];

      var formElement = React.createElement(
        Form,
        {
          schema: app.form.schema,
          onChange: () => {console.log("changed")},
          onSubmit: () => {console.log("submitted")},
          onError: (errors) => {console.log("errors", errors)}
        }
      );

      console.log(formElement);

      ReactDOM.render(formElement, document.getElementById("schemaform"));
    });
  }
}

// initialize vue.js
var vm = new Vue({
  el: '#app',
  data: app,
  methods: {
    findPublicServiceByDestinationId: function() {
      findPublicServiceByDestinationId(app.destinationId);
    },
    selectTab: selectTab,
    selectService: selectService,
    renderSchemaByUri: function() {
      renderSchemaByUri(app.schemaUri);
    },
    findPublicServiceByRoutingApi: findPublicServiceByRoutingApi
  },
  computed: {
    locationHref: function () {
      return location.href;
    }
  }
});
