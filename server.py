from flask import Flask, request, abort, session
from fitconnect import FITConnectClient, Environment
from strictyaml import load
import requests
import os

# read config
with open('config.yaml') as file:
    config = load(file.read()).data

# initialize SDK
fitc = FITConnectClient(Environment[config['sdk']['environment']], config['sdk']['client_id'], config['sdk']['client_secret'])

if not os.path.isfile('server.cfg'):
    print ('aa')
    with open("server.cfg", "a") as file:
        file.write("SECRET_KEY = " + str(os.urandom(16)))

app = Flask(__name__)
app.config.from_pyfile('musterformular.cfg')

# success response
RESPONSE_SUCCESS = {
    'status': 'ok'
}

ROUTING_API_URL = 'https://routing-api-testing.fit-connect.fitko.dev/v1'

@app.route('/api/v1/destinations/<destination_id>')
def get_destination(destination_id):
    print("destination", destination_id)
    r = fitc.get_destination(destination_id)
    if r.status_code == 404:
        return {
            'status': 'not_found'
        }
    elif r.status_code != 200:
        return {
            'status': 'unknown_error'
        }

    return {
        'status': 'ok',
        'destination': r.json()
    }

@app.route('/api/v1/routes')
def get_routes():
    ars = request.args.get('ars')
    leika_key = request.args.get('leikaKey')

    r = requests.get(ROUTING_API_URL + f'/routes?ars={ars}&leikaKey={leika_key}')
    return r.json()

@app.route('/')
def index():
    return app.send_static_file('index.html')
